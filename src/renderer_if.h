/*
 * renderer.h
 *
 *  Created on: Jul 20, 2023
 *      Author: a127590
 */

#ifndef SRC_RENDERER_IF_H_
#define SRC_RENDERER_IF_H_

#include "SDL.h"

class cShape;
class cColor;
class cGroup;
class cTextured;
class cMedia;

class cRendererIf // Visitor
{
    public:
    virtual ~cRendererIf(){};

    public:
    virtual void renderShape(cShape *shape) const = 0;
    virtual void renderColor(cColor *color) const = 0;
    virtual void renderBox(cGroup *box) const = 0;
    virtual void renderTextured(cTextured *textured) const = 0;
    virtual void renderMedia(cMedia *media) const = 0;
};

#endif /* SRC_RENDERER_IF_H_ */
