/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "space_ship.h"

#include <iostream>

using namespace std;

cSpaceShip::cSpaceShip(
        std::shared_ptr<cColor> pColorKey,
        int x, int y,
        int width, int height,
        cPositionable::tPositionReference positionReference
) :
        cMedia("res/spaceshipsprites.png"s, pColorKey, NULL, x, y, width, height, positionReference)
{
    addSpriteClip({0, 0, getWidth()/3, getHeight()/3});
    addSpriteClip({getWidth()/3, 0, getWidth()/3, getHeight()/3});
    addSpriteClip({2*getWidth()/3, 0, getWidth()/3, getHeight()/3});

    setCurrentSpriteClip(1);
}

cSpaceShip::~cSpaceShip()
{
};

