#include "timer.h"

cTimer::cTimer()
{
    mStartTicks = 0;
    mPausedTicks = 0;

    mState = TMR_INITIALIZED;
}

void cTimer::start()
{
    mState = TMR_RUNNING;

    // Get the current clock time
    mStartTicks = SDL_GetTicks();
    mPausedTicks = 0;
}

void cTimer::stop()
{
    // Stop the timer
    mState = TMR_STOPPED;

    // Clear tick variables
    mStartTicks = 0;
    mPausedTicks = 0;
}

void cTimer::pause()
{
    // If the timer is running and isn't already paused
    if( TMR_RUNNING == mState )
    {
        // Pause the timer
        mState = TMR_PAUSED;

        // Calculate the paused ticks
        mPausedTicks = SDL_GetTicks() - mStartTicks;
        mStartTicks = 0;
    }
}

void cTimer::resume()
{
    //If the timer is paused
    if( TMR_PAUSED == mState )
    {
        mState = TMR_RUNNING;

        mStartTicks = SDL_GetTicks() - mPausedTicks;

        mPausedTicks = 0;
    }
}

Uint32 cTimer::getTicks()
{
    if( TMR_PAUSED == mState )
        return mPausedTicks;

    if( TMR_RUNNING == mState )
        return SDL_GetTicks() - mStartTicks;

    return 0;
}

bool cTimer::isStarted()
{
    // Timer has already been started once in its life.
    return TMR_INITIALIZED != mState && TMR_STOPPED != mState;
}

bool cTimer::isRunning()
{
    return TMR_RUNNING == mState;
}

bool cTimer::isPaused()
{
    return TMR_PAUSED == mState;
}

const bool cTimer::isValid() const
{
    return true;
}
