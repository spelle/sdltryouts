/*
 * media.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_BALL_H_
#define SRC_BALL_H_

#include "media.h"

class cBall : public cMedia
{
    private:
    bool moveBall = false;
    int speedBall = 1,
        dirBallX = -1,
        dirBallY = -1;

    public:
    cBall(
            int x = 0, int y = 0,
            int width = -1, int height = -1,
            cPositionable::tPositionReference positionReference = cPositionable::CENTER
    );
    virtual ~cBall();

    void toggleMove();

    void incSpeed();
    void decSpeed();
    void setSpeed(int speed);
    int getSpeed();

    void setDirX(int dir);
    void setDirY(int dir);
    int getDirX();
    int getDirY();

    void move();
};

#endif /* SRC_MEDIA_H_ */
