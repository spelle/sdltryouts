/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "ball.h"

using namespace std;

cBall::cBall(
        int x, int y,
        int width, int height,
        cPositionable::tPositionReference positionReference
) : cMedia("res/ball.png", cColor::clrLightCyan, NULL, x, y, width, height, positionReference)
{
}

cBall::~cBall()
{
}

void cBall::toggleMove()
{
    moveBall = !moveBall;
}

void cBall::incSpeed()
{
    ++speedBall;
}

void cBall::decSpeed()
{
    if (speedBall > 0)
        --speedBall;
}

void cBall::setSpeed(int speed)
{
    speedBall = speed;
}

int cBall::getSpeed()
{
    return speedBall;
}

void cBall::setDirX(int dir)
{
    dirBallX = dir;
}

void cBall::setDirY(int dir)
{
    dirBallY = dir;
}

int cBall::getDirX()
{
    return dirBallX;
}

int cBall::getDirY()
{
    return dirBallY;
}

void cBall::move()
{
    if (moveBall == true)
    {
        setX(getX()+dirBallX*speedBall);
        setY(getY()+dirBallY*speedBall);
    }
}

