#ifndef SRC_TIMER_H
#define SRC_TIMER_H

#include "entity.h"

#include "SDL.h"

class cTimer : public cEntity
{
    public:
    cTimer();

    void start();
    void pause();
    void resume();
    void stop();

    Uint32 getTicks();

    bool isStarted();
    bool isRunning();
    bool isPaused();

    private:
    Uint32 mStartTicks;
    Uint32 mPausedTicks;

    typedef enum {
        TMR_INITIALIZED,
        TMR_RUNNING,
        TMR_PAUSED,
        TMR_STOPPED,
    } cTimerStatus;

    cTimerStatus mState;

    // --- cRenderable overloads ---
    public:
    virtual const bool isValid() const override;
};

#endif // SRC_TIMER_H
