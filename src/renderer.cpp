/*
 * renderer.cpp
 *
 *  Created on: Jul 20, 2023
 *      Author: a127590
 */
#include "renderer.h"

#include "textured.h"
#include "shape.h"
#include "color.h"
#include "group.h"
#include "media.h"
#include "brick.h"

cRenderer::cRenderer()
{
};

cRenderer::~cRenderer()
{
    SDL_DestroyRenderer(mpRndrr);
    mpRndrr = NULL;
};

SDL_Renderer* cRenderer::getRenderer() const
{
    return mpRndrr;
}

void cRenderer::setRenderer(SDL_Renderer* pRndrr)
{
    this->mpRndrr = pRndrr;
}

void cRenderer::renderColor(cColor* color) const
{
    if(!color->isValid() )
        return;

    SDL_SetRenderDrawBlendMode(mpRndrr, (color->getAlpha() == 255) ? SDL_BLENDMODE_NONE : SDL_BLENDMODE_BLEND);
    SDL_SetRenderDrawColor( mpRndrr, color->getRed(), color->getGreen(), color->getBlue(), color->getAlpha() );
}

void cRenderer::renderShape(cShape* shape) const
{
    shape->getColor()->render(this);
    SDL_Point xy ;
    shape->getTopLeftXY(xy);
    switch(shape->getShape())
    {
        case cShape::sLine: {
            //Draw blue horizontal line
            SDL_RenderDrawLine( mpRndrr, xy.x, xy.y, shape->getWidth(), shape->getHeight() );
        } break;
        case cShape::sDotedLine: {
            // TODO : enhance feature
            for( int i = 0; i < shape->getHeight(); i += 8 )
            {
                SDL_RenderDrawLine( mpRndrr, xy.x, xy.y + i, xy.x, xy.y + i - 4 );
            }
        } break;
        case cShape::sRectFilled: {
            SDL_Rect fillRect = { xy.x, xy.y, shape->getWidth(), shape->getHeight() };
            SDL_RenderFillRect( mpRndrr, &fillRect );
        } break;
        case cShape::sRectOutlined: {
            SDL_Rect outlineRect = { xy.x, xy.y, shape->getWidth(), shape->getHeight() };
            SDL_RenderDrawRect( mpRndrr, &outlineRect );
        } break;
        case cShape::sCircleOutlined:
            {
                int x0 = xy.x + shape->getWidth()/2, y0 = xy.y + shape->getHeight()/2, radius = shape->getWidth()/2,
                x = radius-1, y = 0, dx = 1, dy = 1, err = dx - (radius << 1);

                while (x >= y)
                {
                    SDL_RenderDrawPoint(mpRndrr, x0 + x, y0 + y);
                    SDL_RenderDrawPoint(mpRndrr, x0 + y, y0 + x);
                    SDL_RenderDrawPoint(mpRndrr, x0 - y, y0 + x);
                    SDL_RenderDrawPoint(mpRndrr, x0 - x, y0 + y);
                    SDL_RenderDrawPoint(mpRndrr, x0 - x, y0 - y);
                    SDL_RenderDrawPoint(mpRndrr, x0 - y, y0 - x);
                    SDL_RenderDrawPoint(mpRndrr, x0 + y, y0 - x);
                    SDL_RenderDrawPoint(mpRndrr, x0 + x, y0 - y);

                    if (err <= 0)
                    {
                        y++;
                        err += dy;
                        dy += 2;
                    }

                    if (err > 0)
                    {
                        x--;
                        dx += 2;
                        err += dx - (radius << 1);
                    }
                }
            }
            break;
        case cShape::sCircleFilled:
            {
                int x0 = xy.x + shape->getWidth()/2, y0 = xy.y + shape->getHeight()/2, radius = shape->getWidth()/2,
                x = radius-1, y = 0, dx = 1, dy = 1, err = dx - (radius << 1);

                while (x >= y)
                {
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 + x, y0 + y);
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 + y, y0 + x);
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 - y, y0 + x);
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 - x, y0 + y);
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 - x, y0 - y);
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 - y, y0 - x);
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 + y, y0 - x);
                    SDL_RenderDrawLine(mpRndrr, x0, y0, x0 + x, y0 - y);

                    if (err <= 0)
                    {
                        y++;
                        err += dy;
                        dy += 2;
                    }

                    if (err > 0)
                    {
                        x--;
                        dx += 2;
                        err += dx - (radius << 1);
                    }
                }
            }
            break;
        default:
            break;
    }
}


void cRenderer::renderBox(cGroup* box) const
{
    for (auto r : box->getContent())
        r->render(this);
}

void cRenderer::renderTextured(cTextured* txtr) const
{
    SDL_Point xy ;
    txtr->getTopLeftXY(xy);
    SDL_Rect dst = { xy.x, xy.y, txtr->getWidth(), txtr->getHeight() };
    SDL_RenderCopy(mpRndrr, txtr->getTexture(), NULL, &dst);
}

void cRenderer::renderMedia(cMedia *media) const
{
    if( media->hasSpriteClip() )
    {
        SDL_Point xy ;
        media->getTopLeftXY(xy);

        SDL_Rect src = media->getCurrentSpriteClip();
        SDL_Rect dst = { xy.x, xy.y, src.w, src.h };
        //Render to screen
        SDL_RenderCopy( mpRndrr, media->getTexture(), &src, &dst );
    }
    else
    {
        renderTextured(media);
    }
}
