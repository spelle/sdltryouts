/*
 * positionable.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "clickable.h"

cClickable::cClickable(
    int x, int y,
    int width, int height,
    cPositionable::tPositionReference positionReference
) :
    cPositionable(x, y, width, height, positionReference)
{
}

cClickable::~cClickable()
{
};

void cClickable::click(tClickType type, int x, int y)
{
    mClickType = type;
    mClickPos.x = x;
    mClickPos.y = y;
}

const bool cClickable::isClickable() const
{
    return true;
}

bool cClickable::clickDown(const SDL_Point mousePosition)
{
    SDL_Point xy ; getTopLeftXY(xy);
    if (mousePosition.x >= xy.x && mousePosition.x <= xy.x+getWidth() &&
        mousePosition.y >= xy.y && mousePosition.x <= xy.y+getHeight())
    {
        onClick();
        return true;
    }
    return false;
}

bool cClickable::clickUp(SDL_Point mousePosition)
{
    SDL_Point xy ; getTopLeftXY(xy);
    if (mousePosition.x >= xy.x && mousePosition.x <= xy.x+getWidth() &&
        mousePosition.y >= xy.y && mousePosition.x <= xy.y+getHeight())
    {
        onRelease();
        return true;
    }
    return false;
}

const bool cClickable::isValid() const
{
    // FIXME : perhaps shall we do some checks ?
    return true ;
}
