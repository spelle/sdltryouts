/*
 * brick.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "brick.h"

using namespace std;

cBrick::cBrick(
        int touchCount,
        cFont* pFont,
        std::shared_ptr<cColor> pColor,
        std::shared_ptr<cColor> pFontColor,
        int x, int y,
        int width, int height,
        cPositionable::tPositionReference positionReference
) : mTouchCount(touchCount), cPositionable(x, y, width, height, positionReference)
{
        mpShape = new cShape(cShape::sRectFilled, pColor, x, y, width, height, positionReference);
        mpText = new cText(to_string(mTouchCount), pFont, pFontColor, x, y, positionReference);
        if (mTouchCount <= 0)
            mIsBroken = false;
}

cBrick::~cBrick()
{
}

void cBrick::decTouchCount()
{
    if (mTouchCount > 0)
    {
        --mTouchCount;
        mpText->setText(to_string(mTouchCount));
    }

    if (mTouchCount <= 0)
        mIsBroken = true;
}

const bool cBrick::isBroken() const
{
    return mIsBroken;
}

void cBrick::render(const cRendererIf* pRndrrIf)
{
    if (mIsBroken == false)
    {
        pRndrrIf->renderShape(this->mpShape);
        pRndrrIf->renderTextured(this->mpText);
    }
}

const bool cBrick::isPositionnable() const
{
    return true;
}

const bool cBrick::isRenderable() const
{
    return mIsBroken == false;
}

const bool cBrick::isValid() const
{
    // FIXME : perhaps shall we do some checks ?
    return true ;
}
