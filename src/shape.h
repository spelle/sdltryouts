/*
 * shape.h
 *
 *  Created on: Jun 30, 2023
 *      Author: a127590
 */

#ifndef SRC_SHAPE_H_
#define SRC_SHAPE_H_

#include <string>

#include "SDL.h"

#include "entity.h"
#include "clickable.h"
#include "renderer_if.h"
#include "color.h"

class cShape : public cClickable
{
    public:
    typedef enum {
        sLine,
        sDotedLine,
        sRectOutlined,
        sRectFilled,
        sCircleOutlined,
        sCircleFilled,
    } tShape;

    private:
    tShape mShape ;

    std::shared_ptr<cColor> mpColor ;

    public:
    cShape(
        tShape shape,
        std::shared_ptr<cColor> pColor,
        int x=0, int y=0,
        int width=0, int height=0,
        cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
    );
    virtual ~cShape();

    const std::shared_ptr<cColor> getColor() const ;
    const tShape  getShape() const ;

    // --- cClickable overloads ---
    virtual void onClick() override;
    virtual void onRelease() override;

    public:
    virtual const bool isRenderable() const override;

    public:
    virtual void render(const cRendererIf* pRndrrIf) override;

    public:
    virtual const bool isValid() const override;
};

#endif /* SRC_SHAPE_H_ */
