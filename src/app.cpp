//============================================================================
// Name        : anSdlGame.cpp
// Author      : Staplr
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "app.h"

#include <iostream>
#include <stdio.h>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

#include "group.h"
#include "renderer.h"

#include "shape.h"
#include "space_ship.h"

using namespace std;

cApp::cApp(string aName)
: name(aName), screenWidth(1280), screenHeight(768), running(true), window(NULL), renderer(NULL) //, windowSurface(NULL)
{
    if( false == doInit() )
        exit(1);
}

bool cApp::doInit()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        cout << "Couldn't initialize SDL: " << SDL_GetError() << endl;
        return false;
    }

    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

    int windowFlags = SDL_WINDOW_SHOWN;
    window = SDL_CreateWindow( name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight, windowFlags );
    if (!window)
    {
        cout << "Failed to open " << screenWidth << " x " << screenHeight << " window: " << SDL_GetError() << endl;
        return false;
    }

    //Create renderer for window
    int rendererFlags = SDL_RENDERER_ACCELERATED;
    renderer = SDL_CreateRenderer( window, -1, rendererFlags );
    if( renderer == NULL )
    {
        cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << endl ;
        return false;
    }
    cRenderer::getInstance()->setRenderer(renderer);
    //Initialize renderer color
    SDL_SetRenderDrawColor( cRenderer::getInstance()->getRenderer(), 0xFF, 0xFF, 0xFF, 0xFF );

    //Initialize PNG loading
    int imgFlags = IMG_INIT_JPG | IMG_INIT_PNG;
    if( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
        cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << endl;
        return false;
    }

    //Initialize SDL_ttf
    if( TTF_Init() == -1 )
    {
        cout << "SDL_ttf could not initialize! SDL_ttf Error: " << TTF_GetError() << endl;
        return false;
    }

    //Initialize SDL_mixer
    if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
    {
        cout << "SDL_mixer could not initialize! SDL_mixer Error: " << Mix_GetError() << endl;
        return false;
    }

    entities.push_back( new cMedia("res/wave.png", NULL, NULL, 0, 0, screenWidth, screenHeight) );

    mpFont = new cFont("res/kenvector_future.ttf", 14);
    entities.push_back(mpFont);

    cShape* filledRect      = new cShape( cShape::sRectFilled, cColor::clrRed, screenWidth/4, screenHeight/4, screenWidth/2, screenHeight/2 );
    entities.push_back(filledRect);
    cShape* outlinedRect    = new cShape( cShape::sRectOutlined, cColor::clrGreen, screenWidth / 6, screenHeight / 6, screenWidth * 2 / 3, screenHeight * 2 / 3 );
    entities.push_back(outlinedRect);
    cShape* blueHLine       = new cShape( cShape::sLine, cColor::clrBlue, 0, screenHeight / 2, screenWidth, screenHeight / 2);
    entities.push_back(blueHLine);
    cShape* yellowDotedLine = new cShape( cShape::sDotedLine, cColor::clrYellow, screenWidth / 2, 0, screenWidth / 2, screenHeight );
    entities.push_back(yellowDotedLine);
    cShape* whiteCircleOutlined = new cShape( cShape::sCircleOutlined, cColor::clrWhite, screenWidth/2, screenHeight/2, 100, 100, cPositionable::CENTER );
    entities.push_back(whiteCircleOutlined);
    cShape* whiteCircleFilled = new cShape( cShape::sCircleFilled, cColor::clrWhite, screenWidth/4, screenHeight/4, 100, 100, cPositionable::CENTER );
    entities.push_back(whiteCircleFilled);
    cGroup* group = new cGroup( screenWidth-10, 10, 200, 100, cPositionable::TOP_RIGHT );
    entities.push_back(group);
    SDL_Point boxXY ; group->getPosition(boxXY, cPositionable::CENTER);
    group->addContent(new cText( "1", mpFont, cColor::clrWhite, boxXY.x, boxXY.y, 0, 0, cPositionable::CENTER));

    cText* text = new cText( "Hello World !", mpFont, cColor::clrBlack, screenWidth/2, screenHeight/2);
    entities.push_back(text);

    bricks.push_back(new cBrick(1, mpFont, cColor::clrGreen, cColor::clrWhite, screenWidth/8, screenHeight/4, 100, 50, cPositionable::CENTER));

    for (auto b : bricks)
        entities.push_back(b);

    racket = new cMedia("res/vaus.png", cColor::clrLightCyan, NULL, screenWidth/2, screenHeight - 20);
    //racket->setBlendMode(SDL_BLENDMODE_BLEND);
    //racket->setAlpha(255);
    entities.push_back(racket);

    SDL_Point racketPos;
    racket->getPosition(racketPos, cPositionable::TOP_CENTER);
    ball = new cBall(racketPos.x, racketPos.y);
    ball->setPosition({racketPos.x, racketPos.y-ball->getHeight()}, cPositionable::TOP_CENTER);
    //ball->setBlendMode(SDL_BLENDMODE_BLEND);
    //ball->setAlpha(127);
    entities.push_back(ball);

    spaceship = new cSpaceShip(cColor::clrMagenta, screenWidth/2, screenHeight*3/4, -1, -1, cPositionable::TOP_CENTER);
    entities.push_back(spaceship);

    cptrText = new cText("placeholder", mpFont, cColor::clrBlack, screenWidth-500, screenHeight-20);
    entities.push_back(cptrText);
    mousePosText = new cText("X ; Y", mpFont, cColor::clrBlack, 0, screenHeight-20);
    entities.push_back(mousePosText);
    scoreText = new cText("0", mpFont, cColor::clrGreen, 10, 10);
    entities.push_back(scoreText);

    //Load music
    gMedium = Mix_LoadWAV( "res/medium.wav" );
    if( gMedium == NULL )
        cout << "Failed to load beat music! SDL_mixer Error: " << Mix_GetError() << endl;

    //Start counting frames per second
    fpsTimer.start();

    //movingMedia->scale(0.1);
    return true;
}

int cApp::doExecute()
{
    if(running == false) {
        return -1;
    }

    while(running) {
        doInput();

        doLoop();

        doRender();
    }

    return 0;
}

void cApp::doInput()
{
    SDL_Event event;

    while(SDL_PollEvent(&event)) {
        switch(event.type)
        {
        case SDL_QUIT:
            running = false;
            break;
        case SDL_KEYDOWN:
            switch( event.key.keysym.sym )
            {
            case SDLK_UP:
                cout << "UP !" << endl;
                ball->incSpeed();
                break;
            case SDLK_DOWN:
                cout << "DOWN !" << endl;
                ball->decSpeed();
                break;
            case SDLK_LEFT:
                cout << "LEFT !" << endl;
                break;
            case SDLK_RIGHT:
                cout << "RIGHT !" << endl;
                break;
            case SDLK_ESCAPE:
                cout << "ESCAPE !" << endl;
                break;
            case SDLK_LCTRL:
                cout << "LCTRL !" << endl;
                break;
            case SDLK_SPACE:
                cout << "SPACE !" << endl;
                ball->toggleMove();
                break;
            default:
                break;
            }
            break;
        case SDL_MOUSEMOTION:
            SDL_GetMouseState( &mMousePosition.x, &mMousePosition.y );
            racket->setX(mMousePosition.x);
            break;
        case SDL_MOUSEBUTTONDOWN:
            SDL_GetMouseState( &mMousePosition.x, &mMousePosition.y );
            for( auto e : entities )
                e->clickDown(mMousePosition);
            break;
        case SDL_MOUSEBUTTONUP:
            SDL_GetMouseState( &mMousePosition.x, &mMousePosition.y );
            cout << "Mouse Button Up at {" << mMousePosition.x << "," << mMousePosition.y << "} !" << endl;
            break;
        default:
            break;
        }
    }
}

void cApp::doLoop()
{
    //Clear screen
    SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF );
    SDL_RenderClear( renderer );

    //Start cap timer
    capTimer.start();

    //Calculate and correct fps
    float avgFPS = countedFrames / ( fpsTimer.getTicks() / 1000.f );
    if( avgFPS > 2000000 )
    {
        avgFPS = 0;
    }

    ball->move();
    SDL_Point racketTLXY; racket->getPosition(racketTLXY,cPositionable::TOP_LEFT);
    SDL_Point racketTRXY; racket->getPosition(racketTRXY,cPositionable::TOP_RIGHT);
    SDL_Point ballXY; ball->getPosition(ballXY,cPositionable::CENTER);
    if( ballXY.x <= 0 || ballXY.x >= screenWidth )
        ball->setDirX(ball->getDirX() * -1);

    if( ballXY.y <= 0)
        ball->setDirY(ball->getDirY() * -1);

    if( ballXY.y >= screenHeight)
    {
        ball->setDirY(ball->getDirY() * -1);
        ball->getSpeed() >= 2 ? ball->decSpeed() : ball->setSpeed(1);
        --score;
    }

    if( ballXY.y >= racketTLXY.y && ballXY.x <= racketTRXY.x && ballXY.x >= racketTLXY.x)
    {
        Mix_PlayChannel( -1, gMedium, 0 );
        ball->setDirY(ball->getDirY() * -1);
        ++score;
    }

    scoreText->setText(to_string(score));

    // for (auto brick : bricks) {
    //     if (brick->isBroken() == false)
    //     {
    //         SDL_Point brickTLXY; brick->getPosition(brickTLXY,cPositionable::TOP_LEFT);
    //         SDL_Point brickBRXY; brick->getPosition(brickBRXY,cPositionable::BOTTOM_RIGHT);
    //         if( ballXY.y <= brickBRXY.y && ballXY.y >= brickTLXY.y && ballXY.x >= brickTLXY.x && ballXY.x <= brickBRXY.x)
    //         {
    //             if( ballXY.y - ball->getDirY()*ball->getSpeed() > brickBRXY.y || ballXY.y - ball->getDirY()*ball->getSpeed() < brickTLXY.y )
    //             {
    //                 ball->setDirY(ball->getDirY() * -1);
    //                 brick->decTouchCount();
    //             }
    //             if( ballXY.x - ball->getDirX()*ball->getSpeed() > brickBRXY.x || ballXY.x - ball->getDirX()*ball->getSpeed() < brickTLXY.x )
    //             {
    //                 ball->setDirX(ball->getDirX() * -1);
    //                 brick->decTouchCount();
    //             }
    //         }
    //     }
    // }

    //Set text to be rendered
    timeText.str( "" );
    timeText << "Average Frames Per Second " << avgFPS;

    cptrText->setText(timeText.str());

    mousePosText->setText(to_string(mMousePosition.x) + " ; " + to_string(mMousePosition.y));
}

void cApp::doRender()
{
    //Update the surface
    //SDL_UpdateWindowSurface(window);

    //Clear screen
    //SDL_RenderClear(renderer);

    for( auto e : entities )
        e->render(cRenderer::getInstance());

    //Update screen
    SDL_RenderPresent(renderer);
    ++countedFrames;

    //If frame finished early
    int frameTicks = capTimer.getTicks();
    if( frameTicks < screenTicksPerFrame )
    {
        //Wait remaining time
        SDL_Delay( screenTicksPerFrame - frameTicks );
    }
}

void cApp::doCleanup()
{
    for( auto e : entities )
        delete e;

    Mix_FreeChunk( gMedium );

    SDL_DestroyRenderer(renderer);
    renderer = NULL;

    SDL_DestroyWindow(window);
    window = NULL;

    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

cApp::~cApp(){
    doCleanup();
};
