#ifndef SRC_POSITIONABLE_H
#define SRC_POSITIONABLE_H

#include "entity.h"

class cPositionable : public cEntity
{
    // --- Position Reference ---
    public:
    typedef enum {
        TOP_LEFT,
        TOP_CENTER,
        TOP_RIGHT,
        CENTER_LEFT,
        CENTER,
        CENTER_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_CENTER,
        BOTTOM_RIGHT
    } tPositionReference;

    private:
    tPositionReference mPositionReference = TOP_LEFT;

    public:
    void setPositionReference(tPositionReference positionReference);
    tPositionReference getPositionReference() const;

    // --- Position ---
    private:
    SDL_Point mPosition = {0,0};

    public:
    void setPosition(
    	SDL_Point position,
			tPositionReference positionReference = TOP_LEFT
	  );
    void getPosition(SDL_Point& pos, tPositionReference positionReference = TOP_LEFT) const;
    void setX(int x);
    int getX() const;
    void setY( int Y);
    int getY() const;
    virtual void getTopLeftXY(SDL_Point& xy) const;

    // --- Width & Height ---
    private:
    int mWidth  = 0,
        mHeight = 0;

    public:
    void setWidth(int width);
    int getWidth() const;
    void setHeight(int height);
    int getHeight() const;

    public:
    // --- Constructor & Destructor ---
    cPositionable(
        int x = 0, int y=0,
        int width = 0, int height=0,
        tPositionReference positionReference = TOP_LEFT);
    cPositionable(const cPositionable&)=delete;
    virtual ~cPositionable();

    // --- Inherited from cEntity ---
    public:
    virtual const bool isPositionnable() const override;

    public:
    virtual const bool isValid() const override;
};

#endif // SRC_POSITIONABLE_H
