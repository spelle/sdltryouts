/*
 * media.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_FONT_H_
#define SRC_FONT_H_

#include <string>

#include "SDL.h"
#include <SDL_ttf.h>

#include "entity.h"

class cFont : public cEntity
{
    private:
    std::string   mPath;
    int           mPtSize;
    TTF_Font*     mpFont;

    public:
    cFont(std::string path, int ptsize);
    virtual ~cFont();

    bool initFont();

    TTF_Font* getFont();

    // --- cEntity overloads ---
    public:
    virtual const bool isValid() const override;
};

#endif /* SRC_FONT_H_ */
