/*
 * entity.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "entity.h"

cEntity::cEntity()
{
}

cEntity::~cEntity()
{
};

const bool cEntity::isPositionnable() const
{
    return false;
}

const bool cEntity::isRenderable() const
{
    return false;
}

const bool cEntity::isClickable() const
{
    return false;
}

void cEntity::render(const cRendererIf* pRndrrIf)
{
}

bool cEntity::clickDown(SDL_Point mousePosition)
{
    return false;
}

bool cEntity::clickUp(SDL_Point mousePosition)
{
    return false;
}

const bool cEntity::isValid() const
{
    return false;
}

