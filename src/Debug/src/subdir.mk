################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/app.cpp \
../src/entity.cpp \
../src/media.cpp 

CPP_DEPS += \
./src/app.d \
./src/entity.d \
./src/media.d 

OBJS += \
./src/app.o \
./src/entity.o \
./src/media.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src

clean-src:
	-$(RM) ./src/app.d ./src/app.o ./src/entity.d ./src/entity.o ./src/media.d ./src/media.o

.PHONY: clean-src

