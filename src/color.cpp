/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */



/*
 * entity.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#include "color.h"

std::shared_ptr<cColor> cColor::clrRed        = std::make_shared<cColor>( 253,   3,   2, 210 ); // Red
std::shared_ptr<cColor> cColor::clrGreen      = std::make_shared<cColor>(   0, 254,  30, 210 ); // Green
std::shared_ptr<cColor> cColor::clrYellow     = std::make_shared<cColor>( 254, 254,  31, 210 ); // Yellow
std::shared_ptr<cColor> cColor::clrBlue       = std::make_shared<cColor>(  18,  14, 252, 127 ); // Blue
std::shared_ptr<cColor> cColor::clrBlack      = std::make_shared<cColor>(   0,   0,   0, 210 ); // Black
std::shared_ptr<cColor> cColor::clrWhite      = std::make_shared<cColor>( 255, 255, 255, 210 ); // White
std::shared_ptr<cColor> cColor::clrLightCyan  = std::make_shared<cColor>(   0, 255, 255, 210 ); // LightCyan
std::shared_ptr<cColor> cColor::clrMagenta    = std::make_shared<cColor>( 255,   0, 255, 210 ); // Magenta

void cColor::render(const cRendererIf* pRndrrIf)
{
    pRndrrIf->renderColor(this);
}

cColor::cColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)
    : mRed(red), mGreen(green), mBlue(blue), mAlpha(alpha)
{
}

cColor::~cColor()
{
};

Uint8 cColor::getRed() const
{
    return mRed;
}

Uint8 cColor::getGreen() const
{
    return mGreen;
}

Uint8 cColor::getBlue() const
{
    return mBlue;
}

Uint8 cColor::getAlpha() const
{
    return mAlpha;
}

const bool cColor::isRenderable() const
{
    return false;
}

const bool cColor::isValid() const
{
    return true;
}

