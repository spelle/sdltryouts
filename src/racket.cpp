/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "racket.h"

using namespace std;

cRacket::cRacket(
        int x, int y,
        int width, int height,
        cPositionable::tPositionReference positionReference
) : cMedia("res/vaus.png", cColor::clrLightCyan, NULL, x, y, width, height, positionReference)
{
}

cRacket::~cRacket()
{
};
