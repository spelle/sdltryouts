/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "media.h"
#include "renderer.h"

#include <iostream>

#include <SDL_image.h>

using namespace std;

cMedia::cMedia(
        string path,
        std::shared_ptr<cColor> pColorKey,
        std::shared_ptr<cColor> pColorMod,
        int x, int y,
        int width, int height,
        cPositionable::tPositionReference positionReference
) :
        cTextured(pColorMod, x, y, width, height, positionReference),
        mPath(path),
        mpColorKey(pColorKey),
        mCurrentSpriteClip(0)
{
    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load( path.c_str() );
    if( loadedSurface == NULL )
    {
        cout << "Couldn't load surface for " << mPath << endl;
        return ;
    }

    //Color key image
    if( mpColorKey != NULL )
        SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, mpColorKey->getRed(), mpColorKey->getGreen(), mpColorKey->getBlue() ) );

    //Create texture from surface pixels
    mpTxtr = SDL_CreateTextureFromSurface( cRenderer::getInstance()->getRenderer(), loadedSurface );

    //Get image dimensions
    setWidth(loadedSurface->w);
    setHeight(loadedSurface->h);

    updateTexture();

    //Get rid of old loaded surface
    SDL_FreeSurface( loadedSurface );
}

cMedia::~cMedia()
{
    //Free loaded image
    SDL_DestroyTexture(mpTxtr);
    mpTxtr = NULL;
};

void cMedia::setColorKey(std::shared_ptr<cColor> clrKey)
{
    mpColorKey = clrKey;
    updateTexture();
}

void cMedia::getTopLeftXY(SDL_Point& xy) const
{
    cPositionable::getTopLeftXY(xy);
    if( hasSpriteClip() )
    {
        SDL_Rect clip = getCurrentSpriteClip() ;
        xy.x += clip.w ;
        xy.y += clip.h ;
    }
}

void cMedia::addSpriteClip(SDL_Rect spriteClip)
{
    mSpriteClips.push_back(spriteClip);
}

bool cMedia::hasSpriteClip() const
{
    return !mSpriteClips.empty();
}

SDL_Rect cMedia::getSpriteClip(size_t index) const
{
    return mSpriteClips[index];
}

SDL_Rect cMedia::getCurrentSpriteClip() const
{
    return mSpriteClips[mCurrentSpriteClip];
}

void cMedia::setCurrentSpriteClip(size_t index)
{
    mCurrentSpriteClip = index;
}

size_t cMedia::getSpriteClipSize() const
{
    return mSpriteClips.size();
}

const bool cMedia::isRenderable() const
{
    return true;
}

void cMedia::render(const cRendererIf* pRndrrIf)
{
    pRndrrIf->renderMedia(this);
}

const bool cMedia::isValid() const
{
    return true ;
}

