/*
 * singleton.h
 *
 *  Created on: Jul 20, 2023
 *      Author: a127590
 */

#ifndef SRC_SINGLETON_H_
#define SRC_SINGLETON_H_

template<typename T>
class cSingleton
{
    protected:
    cSingleton(){};
    virtual ~cSingleton(){};

    private:
    cSingleton(const cSingleton&) = delete;
    cSingleton& operator=(const cSingleton& other) = delete;

    private:


    public:
    static T* getInstance()
    {
        static T* instance=nullptr;

        if(instance==nullptr){
            instance = new T();
        }
        return instance;
    };
};

#endif /* SRC_SINGLETON_H_ */
