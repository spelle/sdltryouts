#ifndef SRC_APP_H
#define SRC_APP_H

#include <string>
#include <vector>
#include <sstream>

#include "shape.h"
#include "media.h"
#include "brick.h"
#include "ball.h"
#include "text.h"
#include "timer.h"

#include <SDL_mixer.h>

class cApp
{
private:
    std::string name;
    int  screenWidth;
    int screenHeight;
    int screenFps = 60;
    int screenTicksPerFrame = 1000 / screenFps;
    bool     running;

    SDL_Window * window;
    //SDL_Texture* texture;
    SDL_Renderer* renderer;

    SDL_Point mMousePosition = {0,0};

    std::vector<cEntity*> entities;
    //std::vector<cRenderable*> renderables;

    cMedia* racket = NULL;

    std::vector<cBrick*> bricks;

    cBall* ball = NULL;
    cMedia* spaceship = NULL;
    int score = 0;
    cTimer fpsTimer;
    cTimer capTimer;
    int countedFrames = 0;

    cFont* mpFont;

    //In memory text stream
    std::stringstream timeText;

    cText* cptrText = NULL;
    cText* mousePosText = NULL;
    cText* scoreText = NULL;
    Mix_Chunk* gMedium;

public:
    cApp(std::string aName);
    int doExecute();
    virtual ~cApp();

private:
    bool doInit();
    void doInput();
    void doLoop();
    void doRender();
    void doCleanup();
};

#endif // SRC_APP_H
