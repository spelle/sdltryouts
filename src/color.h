/*
 * shape.h
 *
 *  Created on: Jun 30, 2023
 *      Author: a127590
 */

#ifndef SRC_COLOR_H_
#define SRC_COLOR_H_

#include "entity.h"

#include "SDL.h"

class cColor : public cEntity
{
    public:
    static std::shared_ptr<cColor> clrRed       ;
    static std::shared_ptr<cColor> clrGreen     ;
    static std::shared_ptr<cColor> clrYellow    ;
    static std::shared_ptr<cColor> clrBlue      ;
    static std::shared_ptr<cColor> clrBlack     ;
    static std::shared_ptr<cColor> clrWhite     ;
    static std::shared_ptr<cColor> clrLightCyan ;
    static std::shared_ptr<cColor> clrMagenta   ;

    private:
    Uint8 mRed, mGreen, mBlue, mAlpha;

    public:
    cColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha);
    virtual ~cColor();

    Uint8 getRed() const;
    Uint8 getGreen() const;
    Uint8 getBlue() const;
    Uint8 getAlpha() const;

    // --- cEntity overloads ---
    public:
    virtual const bool isRenderable() const override;

    public:
    virtual void render(const cRendererIf* pRndrrIf) override;

    public:
    virtual const bool isValid() const override;
};

#endif /* SRC_COLOR_H_ */
