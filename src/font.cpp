/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */



/*
 * entity.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "font.h"

#include <SDL_ttf.h>

#include <iostream>

using namespace std;

cFont::cFont(string path, int ptsize)
: mPath(path), mPtSize(ptsize),mpFont(NULL)
{
    //Open the font
    mpFont = TTF_OpenFont( mPath.c_str(), mPtSize );
    if( mpFont == NULL )
    {
        cout << "Failed to load lazy font! SDL_ttf Error: " << TTF_GetError() << endl;
    }
}

TTF_Font* cFont::getFont()
{
    return mpFont ;
}

cFont::~cFont()
{
    TTF_CloseFont( mpFont );
    mpFont = NULL;
};

const bool cFont::isValid() const
{
    return true ;
}
