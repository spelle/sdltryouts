/*
 * box.cpp
 *
 *  Created on: Jul 11, 2023
 *      Author: a127590
 */

#include "group.h"

#include <iostream>

cGroup::cGroup(
    int x, int y,
    int width, int height,
    cPositionable::tPositionReference positionReference
):
    cClickable(x, y, width, height, positionReference)
{
}

cGroup::~cGroup()
{
}

void cGroup::addContent(cEntity * pContent)
{
    if( NULL != pContent)
        mContent.push_back(pContent);
}

std::vector<cEntity*> cGroup::getContent() const
{
    return mContent;
}

void cGroup::onClick()
{
    std::cout << "cGroup::onClick" << std::endl;
}

void cGroup::onRelease()
{
    std::cout << "cGroup::onRelease" << std::endl;
}

void cGroup::render(const cRendererIf* pRndrrIf)
{
    pRndrrIf->renderBox(this);
}

const bool cGroup::isValid() const
{
    // TODO
    return true;
}
