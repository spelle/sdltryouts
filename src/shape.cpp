/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */



/*
 * entity.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "shape.h"

#include <SDL_image.h>

#include <iostream>

using namespace std;

cShape::cShape(
    tShape shape,
    std::shared_ptr<cColor> pColor,
    int x,
    int y,
    int width,
    int height,
    cPositionable::tPositionReference positionReference
) :
    cClickable(x, y, width, height, positionReference),
    mShape(shape),
    mpColor(pColor)
{
}

cShape::~cShape()
{
};

const std::shared_ptr<cColor> cShape::getColor() const
{
    return mpColor ;
}
const cShape::tShape  cShape::getShape() const
{
    return mShape ;
}

const bool cShape::isRenderable() const
{
    return true;
}

void cShape::render(const cRendererIf* pRndrrIf)
{
    pRndrrIf->renderShape(this);
}

void cShape::onClick()
{
    std::cout << "cShape::onClick" << std::endl;
}

void cShape::onRelease()
{
    std::cout << "cShape::onRelease" << std::endl;
}

const bool cShape::isValid() const
{
    return true;
}
