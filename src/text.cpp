/*
 * media.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */



/*
 * entity.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "text.h"
#include "renderer.h"

#include <iostream>

using namespace std;

cText::cText(
		string text,
		cFont* pFont,
		std::shared_ptr<cColor> pcolor,
		int x, int y,
		int width, int height,
		cPositionable::tPositionReference positionReference
) :
		cTextured(NULL,x,y,width,height,positionReference),
		mText(text),
		mpFont(pFont),
		mpColor(pcolor)
{
	updateTexture();
}

void cText::updateTexture()
{
    SDL_DestroyTexture(mpTxtr);

    //Render text surface
    SDL_Surface* textSurface = TTF_RenderText_Solid( mpFont->getFont(), mText.c_str(), {mpColor->getRed(), mpColor->getGreen(), mpColor->getBlue(), mpColor->getAlpha()} );
    if( textSurface == NULL )
    {
        cout << "Unable to render text surface! SDL_ttf Error: " << TTF_GetError() << endl;
        return; // TODO : Raise an exception
    }

    //Create texture from surface pixels
    mpTxtr = SDL_CreateTextureFromSurface( cRenderer::getInstance()->getRenderer(), textSurface );
    if( mpTxtr == NULL )
    {
        cout << "Unable to create texture from rendered text! SDL Error: " << SDL_GetError() << endl;
        return; // TODO : Raise an exception
    }

    // Get image dimensions
    setWidth(textSurface->w);
    setHeight(textSurface->h);

    //Get rid of old surface
    SDL_FreeSurface( textSurface );
}

void cText::setText(std::string text)
{
    mText=text;

    updateTexture();
}

cText::~cText()
{
}

const bool cText::isValid() const
{
    return true;// TODO
}

