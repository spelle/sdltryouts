/*
 * positionable.cpp
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */
#include "positionable.h"

cPositionable::cPositionable(
        int x, int y,
        int width, int height,
        tPositionReference positionReference
) :
        mPositionReference(positionReference),
        mPosition({x,y}),
        mWidth(width),
        mHeight(height)
{
};

cPositionable::~cPositionable()
{
};

void cPositionable::setPositionReference(tPositionReference positionReference)
{
    mPositionReference = positionReference;
}

cPositionable::tPositionReference cPositionable::getPositionReference() const
{
    return mPositionReference;
}

void cPositionable::setPosition(SDL_Point position, tPositionReference positionReference)
{
    mPosition=position;
    mPositionReference = positionReference;
}
void cPositionable::getPosition(SDL_Point& pos, tPositionReference positionReference) const
{
    getTopLeftXY(pos);

    int x=pos.x, y=pos.y;
    switch(positionReference)
    {
    case TOP_CENTER:
    x = pos.x + mWidth/2;
    break;
    case TOP_RIGHT:
    x = pos.x + mWidth;
    break;
    case CENTER_LEFT:
    y = pos.y + mHeight/2;
    break;
    case CENTER:
    x = pos.x + mWidth/2;
    y = pos.y + mHeight/2;
    break;
    case CENTER_RIGHT:
    x = pos.x + mWidth;
    y = pos.y + mHeight/2;
    break;
    case BOTTOM_LEFT:
    y = pos.y + mHeight;
    break;
    case BOTTOM_CENTER:
    x = pos.x + mWidth/2;
    y = pos.y + mHeight;
    break;
    case BOTTOM_RIGHT:
    x = pos.x + mWidth;
    y = pos.y + mHeight;
    break;
    default: // TOP_LEFT
    break;
    }

    pos.x = x ; pos.y = y ;
}

void cPositionable::getTopLeftXY(SDL_Point& xy) const
{
	int x=mPosition.x, y=mPosition.y;
	switch(mPositionReference)
	{
	case TOP_CENTER:
		x = mPosition.x - mWidth/2;
		break;
	case TOP_RIGHT:
		x = mPosition.x - mWidth;
		break;
	case CENTER_LEFT:
		y = mPosition.y - mHeight/2;
		break;
	case CENTER:
		x = mPosition.x - mWidth/2;
		y = mPosition.y - mHeight/2;
		break;
	case CENTER_RIGHT:
		x = mPosition.x - mWidth;
		y = mPosition.y - mHeight/2;
		break;
	case BOTTOM_LEFT:
		x = mPosition.x;
		y = mPosition.y-mHeight;
		break;
	case BOTTOM_CENTER:
		x = mPosition.x - mWidth/2;
		y = mPosition.y-mHeight;
		break;
	case BOTTOM_RIGHT:
		x = mPosition.x - mWidth;
		y = mPosition.y - mHeight;
		break;
	default: // TOP_LEFT
		break;
	}

	xy.x = x ; xy.y = y ;
}
void cPositionable::setX(int x) {  mPosition.x = x; }
int cPositionable::getX() const { return mPosition.x; }
void cPositionable::setY(int y) {  mPosition.y = y; }
int cPositionable::getY() const { return mPosition.y; }

void cPositionable::setWidth(int width) { mWidth = width; }
int cPositionable::getWidth() const { return mWidth; }
void cPositionable::setHeight(int height) { mHeight = height; }
int cPositionable::getHeight() const { return mHeight; }

const bool cPositionable::isPositionnable() const
{
    return true;
}

const bool cPositionable::isValid() const
{
    // FIXME : perhaps shall we do some checks ?
    return true ;
}
