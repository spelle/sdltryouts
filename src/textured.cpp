#include "textured.h"

#include <iostream>

cTextured::cTextured(
    std::shared_ptr<cColor> pColorMod,
    int x, int y,
    int width, int height,
    cPositionable::tPositionReference positionReference
) :
    cClickable(x, y, width, height, positionReference),
    mpColorMod(pColorMod)
{
};

cTextured::~cTextured()
{
    // Free the texture
    SDL_DestroyTexture(mpTxtr);
    mpTxtr = NULL;
};

SDL_Texture* cTextured::getTexture() const
{
    return mpTxtr;
}

void cTextured::setColorMod(std::shared_ptr<cColor> pColorMod)
{
    mpColorMod = pColorMod;
    updateTexture();
}

std::shared_ptr<cColor> cTextured::getColorMod()
{
    return mpColorMod;
}

void cTextured::setBlendMode( SDL_BlendMode blendMode )
{
    mBlendMode = blendMode;

    updateTexture();
}

void cTextured::setAlpha( Uint8 alpha )
{
    mAlpha=alpha;

    updateTexture();
}

void cTextured::setScale(float scale) { mScale = scale; }
float cTextured::getScale() { return mScale; }

void cTextured::updateTexture()
{
    //Modulate texture
    if( mpColorMod != NULL )
        SDL_SetTextureColorMod( mpTxtr, mpColorMod->getRed(), mpColorMod->getGreen(), mpColorMod->getBlue() );

    //Set blending function
    SDL_SetTextureBlendMode( mpTxtr, mBlendMode );

    //Modulate texture alpha
    SDL_SetTextureAlphaMod( mpTxtr, mAlpha );
}

void cTextured::onClick()
{
    std::cout << "cTextured::onClick" << std::endl;
}

void cTextured::onRelease()
{
    std::cout << "cTextured::onRelease" << std::endl;
}

const bool cTextured::isRenderable() const
{
    return true;
}

void cTextured::render(const cRendererIf* pRndrrIf)
{
    pRndrrIf->renderTextured(this);
}

const bool cTextured::isValid() const
{
    return mpTxtr != NULL;
}
