/*
 * brick.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_BRICK_H_
#define SRC_BRICK_H_

#include "shape.h"
#include "text.h"
#include "color.h"

class cBrick : public cPositionable
{
    private:
    int mTouchCount = 0;
    bool mIsBroken = false;
    cShape* mpShape = nullptr;
    cText* mpText = nullptr;

    public:
    cBrick(
        int mTouchCount,
        cFont* pFont,
        std::shared_ptr<cColor> pColor = cColor::clrGreen,
        std::shared_ptr<cColor> pFontColor = cColor::clrWhite,
        int x=0, int y=0,
        int width=0, int height=0,
        cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
    );
    virtual ~cBrick();

    void decTouchCount();
    virtual const bool isBroken() const;

    // --- Inherited from cEntity ---
    public:
    virtual const bool isRenderable() const override;

    public:
    virtual void render(const cRendererIf* pRndrrIf) override;

    public:
    virtual const bool isPositionnable() const override;

    public:
    virtual const bool isValid() const override;
};

#endif /* SRC_BRICK_H_ */
