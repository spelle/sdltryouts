cmake_minimum_required(VERSION 3.0.0)
project(sdlTryouts VERSION 0.1.0)

set (SRC_FILES main.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/brick.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/ball.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/app.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/shape.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/color.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/media.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/timer.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/font.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/group.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/positionable.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/clickable.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/racket.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/textured.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/space_ship.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/text.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/entity.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/renderer.cpp
)

add_executable(sdlTryouts ${SRC_FILES})

IF (WIN32)
    target_include_directories(sdlTryouts PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2-2.30.2/include
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_ttf-2.22.0/include
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_image-2.8.2/include
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_mixer-2.8.0/include
        ${CMAKE_CURRENT_SOURCE_DIR}/include)
ELSE()
    target_include_directories(sdlTryouts PUBLIC /usr/include/SDL2 ${CMAKE_CURRENT_SOURCE_DIR}/include)
ENDIF()

target_link_libraries(sdlTryouts
    SDL2
    SDL2_image
    SDL2_ttf
    SDL2_mixer
    SDL2main
)

IF (WIN32)
    target_link_directories(sdlTryouts PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2-2.30.2/lib/x64
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_ttf-2.22.0/lib/x64
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_image-2.8.2/lib/x64
        ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_mixer-2.8.0/lib/x64
    )
ENDIF()


set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)

IF (WIN32)
    file(COPY res DESTINATION ${CMAKE_BINARY_DIR}/Debug)
    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2-2.30.2/lib/x64/SDL2.dll DESTINATION ${CMAKE_BINARY_DIR}/Debug)
    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_ttf-2.22.0/lib/x64/SDL2_ttf.dll DESTINATION ${CMAKE_BINARY_DIR}/Debug)
    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_image-2.8.2/lib/x64/SDL2_image.dll DESTINATION ${CMAKE_BINARY_DIR}/Debug)
    file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/../SDL2_mixer-2.8.0/lib/x64/SDL2_mixer.dll DESTINATION ${CMAKE_BINARY_DIR}/Debug)
ELSE()
    file(COPY res DESTINATION ${CMAKE_BINARY_DIR})
ENDIF()