/*
 * media.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_RACKET_H_
#define SRC_RACKET_H_

#include "media.h"

class cRacket : public cMedia
{
    public:
    cRacket(
            int x = 0, int y = 0,
            int width = -1, int height = -1,
            cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
    );
    virtual ~cRacket();
};

#endif /* SRC_RACKET_H_ */
