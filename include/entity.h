#ifndef SRC_ENTITY_H
#define SRC_ENTITY_H

#include <list>
#include <memory>

#include "SDL.h"

#include "renderer_if.h"

class cEntity
{
    public:
    cEntity();
    virtual ~cEntity();

    public:
    virtual const bool isPositionnable() const;
    virtual const bool isRenderable()    const;
    virtual const bool isClickable()     const;

    public:
    virtual void render(const cRendererIf* pRndrrIf);

    public:
    virtual bool clickDown(SDL_Point mousePosition);
    virtual bool clickUp(SDL_Point mousePosition);

    public:
    virtual const bool isValid() const;
};

#endif // SRC_ENTITY_H
