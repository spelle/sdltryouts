/*
 * media.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_MEDIA_H_
#define SRC_MEDIA_H_

#include <string>
#include <vector>

#include "SDL.h"

#include "textured.h"
#include "color.h"

class cMedia : public cTextured
{
    private:
    std::string   mPath;

    std::shared_ptr<cColor>      mpColorKey;

    std::vector<SDL_Rect> mSpriteClips;
    size_t mCurrentSpriteClip = 0;

    public:
    cMedia(
            std::string path,
            std::shared_ptr<cColor> pColorKey = NULL,
            std::shared_ptr<cColor> pColorMod = NULL,
            int x = 0, int y = 0,
            int width = 0, int height = 0,
            cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
    );
    virtual ~cMedia();

    void setColorKey(std::shared_ptr<cColor> clrKey);

    virtual void getTopLeftXY(SDL_Point& xy) const override;

    public:
    virtual void addSpriteClip(SDL_Rect sprite);
    virtual bool hasSpriteClip() const;
    virtual SDL_Rect getSpriteClip(size_t index) const;
    virtual SDL_Rect getCurrentSpriteClip() const;
    void setCurrentSpriteClip(size_t index);
    virtual size_t getSpriteClipSize() const;

    // --- cEntity overloads ---
    public:
    virtual const bool isRenderable()    const override;

    public:
    virtual void render(const cRendererIf* pRndrrIf) override;

    public:
    virtual const bool isValid() const override;
};

#endif /* SRC_MEDIA_H_ */
