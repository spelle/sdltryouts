#ifndef SRC_TEXTURED_H
#define SRC_TEXTURED_H

#include "clickable.h"

#include "renderer_if.h"

#include "color.h"

#include "SDL.h"

class cTextured : public cClickable
{
    protected:
    SDL_Texture*   mpTxtr  = NULL;

    public:
    SDL_Texture* getTexture() const;

    // --- Scaling ---
    private:
    float mScale = 1.0;

    public:
    void setScale(float scale);
    float getScale();

    // --- Color Modulation ---
    private:
    std::shared_ptr<cColor>      mpColorMod = NULL;

    public:
    void setColorMod(std::shared_ptr<cColor> pColorMod);
    std::shared_ptr<cColor> getColorMod();

    // --- Blend Mode & Transparency ---
    private:
    SDL_BlendMode mBlendMode = SDL_BLENDMODE_BLEND;
    Uint8         mAlpha = 255;

    public:
    void setBlendMode(SDL_BlendMode blending);
    void setAlpha(Uint8 alpha );

    public:
    // --- Constructor & Destructor ---
    cTextured(
            std::shared_ptr<cColor> pColorMod = NULL,
            int x = 0, int y=0,
            int width = 0, int height=0,
            cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
    );
    virtual ~cTextured();

    public:
    virtual void updateTexture();

    // --- cClickable overloads ---
    virtual void onClick() override;
    virtual void onRelease() override;

    // --- cEntity overloads ---
    public:
    virtual const bool isRenderable() const override;

    public:
    virtual void render(const cRendererIf* pRndrrIf) override;

    public:
    virtual const bool isValid() const override;
};

#endif // SRC_TEXTURED_H
