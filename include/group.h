/*
 * group.h
 *
 *  Created on: Jul 11, 2023
 *      Author: a127590
 */

#ifndef SRC_GROUP_H_
#define SRC_GROUP_H_

#include "clickable.h"

#include <vector>

class cGroup: public cClickable
{
    private:
    std::vector<cEntity*> mContent;

    public:
    cGroup(
            int x=0, int y=0,
            int width=0, int height=0,
            cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
    );
    virtual ~cGroup();

    void addContent(cEntity * pContent);
    std::vector<cEntity*> getContent() const;

    // --- cClickable overloads ---
    virtual void onClick() override;
    virtual void onRelease() override;

    // --- cEntity overloads ---
    public:
    virtual void render(const cRendererIf* pRndrrIf) override;

    public:
    virtual const bool isValid() const override;
};

#endif /* SRC_GROUP_H_ */
