/*
 * media.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_SPACE_SHIP_H_
#define SRC_SPACE_SHIP_H_

#include <string>
#include <vector>

#include "media.h"

class cSpaceShip : public cMedia
{
    public:
    cSpaceShip(
            std::shared_ptr<cColor> pColorKey = NULL,
            int x = 0, int y = 0,
            int width = 0, int height = 0,
            cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
    );
    virtual ~cSpaceShip();
};

#endif /* SRC_SPACE_SHIP_H_ */
