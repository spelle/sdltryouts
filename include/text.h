/*
 * media.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_TEXT_H_
#define SRC_TEXT_H_

#include <string>

#include "SDL.h"

#include "textured.h"
#include "font.h"
#include "color.h"

class cText : public cTextured
{
    private:
    std::string  mText;
    cFont*       mpFont=NULL;
    std::shared_ptr<cColor>      mpColor=NULL;

    // --- Setters & Getters ---
    public:
    void setText(std::string text);

    public:
    // --- Constructor & Destructor ---
    cText(
        std::string text,
        cFont* pFont,
        std::shared_ptr<cColor>  pColor,
        int x=0, int y=0,
        int width=0, int height=0,
        cPositionable::tPositionReference positionReference = cPositionable::TOP_LEFT
	);
    virtual ~cText();

    // --- cTextured overloads ---
    public:
    virtual void updateTexture();

    // --- cEntity overloads ---
    public:
    virtual const bool isValid() const override;
};

#endif /* SRC_TEXT_H_ */
