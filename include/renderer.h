/*
 * renderer.h
 *
 *  Created on: Jul 20, 2023
 *      Author: a127590
 */

#ifndef SRC_RENDERER_H_
#define SRC_RENDERER_H_

#include "renderer_if.h"
#include "singleton.h"

#include "SDL.h"

class cRenderer : public cRendererIf, public cSingleton<cRenderer>
{
    public:
    cRenderer();
    virtual ~cRenderer();

    SDL_Renderer * mpRndrr = NULL;

    public:
    SDL_Renderer* getRenderer() const;
    void setRenderer(SDL_Renderer* pRndrr = NULL);

    public:
    virtual void renderShape(cShape *shape) const;
    virtual void renderColor(cColor *color) const;
    virtual void renderBox(cGroup *box) const;
    virtual void renderTextured(cTextured *textured) const;
    virtual void renderMedia(cMedia *media) const;
};

#endif /* SRC_RENDERER_H_ */
