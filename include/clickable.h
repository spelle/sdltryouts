#ifndef SRC_CLICKABLE_H
#define SRC_CLICKABLE_H

#include "positionable.h"

class cClickable : public cPositionable
{
    // --- Click Type ---
    public:
    typedef enum {
        CLICK_CLICKED,
        CLICK_RELEASED,
    } tClickType;

    protected:
    tClickType mClickType = CLICK_CLICKED;
    SDL_Point mClickPos = {0,0};

    public:
    virtual void click(tClickType type, int x, int y) final;

    virtual void onClick() = 0;
    virtual void onRelease() = 0;

    public:
    // --- Constructor & Destructor ---
    cClickable(
        int x = 0, int y=0,
        int width = 0, int height=0,
        tPositionReference positionReference = TOP_LEFT);
    cClickable(const cClickable&) = delete;
    virtual ~cClickable();

    // --- cEntity overloads ---
    public:
    const bool isClickable() const override;

    public:
    virtual bool clickDown(const SDL_Point mousePosition) override;
    virtual bool clickUp(SDL_Point mousePosition) override;

    public:
    virtual const bool isValid() const override;
};

#endif // SRC_CLICKABLE_H
