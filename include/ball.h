/*
 * media.h
 *
 *  Created on: Jun 29, 2023
 *      Author: a127590
 */

#ifndef SRC_BALL_H_
#define SRC_BALL_H_

#include "media.h"

class cBall : public cMedia
{
    public:
    cBall(
            int x = 0, int y = 0,
            int width = -1, int height = -1,
            cPositionable::tPositionReference positionReference = cPositionable::CENTER
    );
    virtual ~cBall();
};

#endif /* SRC_MEDIA_H_ */
